// ==============================================================
// File generated on Tue Feb 25 10:20:14 IST 2020
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xperformance_counter.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XPerformance_counter_CfgInitialize(XPerformance_counter *InstancePtr, XPerformance_counter_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Axilites_BaseAddress = ConfigPtr->Axilites_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XPerformance_counter_Start(XPerformance_counter *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL) & 0x80;
    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL, Data | 0x01);
}

u32 XPerformance_counter_IsDone(XPerformance_counter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XPerformance_counter_IsIdle(XPerformance_counter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XPerformance_counter_IsReady(XPerformance_counter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XPerformance_counter_EnableAutoRestart(XPerformance_counter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL, 0x80);
}

void XPerformance_counter_DisableAutoRestart(XPerformance_counter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_CTRL, 0);
}

u32 XPerformance_counter_Get_return(XPerformance_counter *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_AP_RETURN);
    return Data;
}
void XPerformance_counter_InterruptGlobalEnable(XPerformance_counter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_GIE, 1);
}

void XPerformance_counter_InterruptGlobalDisable(XPerformance_counter *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_GIE, 0);
}

void XPerformance_counter_InterruptEnable(XPerformance_counter *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_IER);
    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_IER, Register | Mask);
}

void XPerformance_counter_InterruptDisable(XPerformance_counter *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_IER);
    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_IER, Register & (~Mask));
}

void XPerformance_counter_InterruptClear(XPerformance_counter *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XPerformance_counter_WriteReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_ISR, Mask);
}

u32 XPerformance_counter_InterruptGetEnabled(XPerformance_counter *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_IER);
}

u32 XPerformance_counter_InterruptGetStatus(XPerformance_counter *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XPerformance_counter_ReadReg(InstancePtr->Axilites_BaseAddress, XPERFORMANCE_COUNTER_AXILITES_ADDR_ISR);
}

