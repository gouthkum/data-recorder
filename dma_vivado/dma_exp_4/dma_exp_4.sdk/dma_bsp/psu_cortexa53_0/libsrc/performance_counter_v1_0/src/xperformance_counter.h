// ==============================================================
// File generated on Tue Feb 25 10:20:14 IST 2020
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XPERFORMANCE_COUNTER_H
#define XPERFORMANCE_COUNTER_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xperformance_counter_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Axilites_BaseAddress;
} XPerformance_counter_Config;
#endif

typedef struct {
    u32 Axilites_BaseAddress;
    u32 IsReady;
} XPerformance_counter;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XPerformance_counter_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XPerformance_counter_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XPerformance_counter_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XPerformance_counter_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XPerformance_counter_Initialize(XPerformance_counter *InstancePtr, u16 DeviceId);
XPerformance_counter_Config* XPerformance_counter_LookupConfig(u16 DeviceId);
int XPerformance_counter_CfgInitialize(XPerformance_counter *InstancePtr, XPerformance_counter_Config *ConfigPtr);
#else
int XPerformance_counter_Initialize(XPerformance_counter *InstancePtr, const char* InstanceName);
int XPerformance_counter_Release(XPerformance_counter *InstancePtr);
#endif

void XPerformance_counter_Start(XPerformance_counter *InstancePtr);
u32 XPerformance_counter_IsDone(XPerformance_counter *InstancePtr);
u32 XPerformance_counter_IsIdle(XPerformance_counter *InstancePtr);
u32 XPerformance_counter_IsReady(XPerformance_counter *InstancePtr);
void XPerformance_counter_EnableAutoRestart(XPerformance_counter *InstancePtr);
void XPerformance_counter_DisableAutoRestart(XPerformance_counter *InstancePtr);
u32 XPerformance_counter_Get_return(XPerformance_counter *InstancePtr);


void XPerformance_counter_InterruptGlobalEnable(XPerformance_counter *InstancePtr);
void XPerformance_counter_InterruptGlobalDisable(XPerformance_counter *InstancePtr);
void XPerformance_counter_InterruptEnable(XPerformance_counter *InstancePtr, u32 Mask);
void XPerformance_counter_InterruptDisable(XPerformance_counter *InstancePtr, u32 Mask);
void XPerformance_counter_InterruptClear(XPerformance_counter *InstancePtr, u32 Mask);
u32 XPerformance_counter_InterruptGetEnabled(XPerformance_counter *InstancePtr);
u32 XPerformance_counter_InterruptGetStatus(XPerformance_counter *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
