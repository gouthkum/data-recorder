// ==============================================================
// File generated on Tue Feb 25 10:20:14 IST 2020
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xperformance_counter.h"

extern XPerformance_counter_Config XPerformance_counter_ConfigTable[];

XPerformance_counter_Config *XPerformance_counter_LookupConfig(u16 DeviceId) {
	XPerformance_counter_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XPERFORMANCE_COUNTER_NUM_INSTANCES; Index++) {
		if (XPerformance_counter_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XPerformance_counter_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XPerformance_counter_Initialize(XPerformance_counter *InstancePtr, u16 DeviceId) {
	XPerformance_counter_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XPerformance_counter_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XPerformance_counter_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

