# aclk {FREQ_HZ 156250000 CLK_DOMAIN design_1_clk_wiz_0_1_clk_out1 PHASE 0.0}
# Clock Domain: design_1_clk_wiz_0_1_clk_out1
create_clock -name aclk -period 6.400 [get_ports aclk]
# Generated clocks
