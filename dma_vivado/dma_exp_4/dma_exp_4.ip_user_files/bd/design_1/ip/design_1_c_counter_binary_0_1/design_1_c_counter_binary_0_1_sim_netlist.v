// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.3 (lin64) Build 2405991 Thu Dec  6 23:36:41 MST 2018
// Date        : Wed Feb 26 13:09:03 2020
// Host        : workstation12 running 64-bit Ubuntu 16.04.5 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/gkumar/dma_exp_4/dma_exp_4.srcs/sources_1/bd/design_1/ip/design_1_c_counter_binary_0_1/design_1_c_counter_binary_0_1_sim_netlist.v
// Design      : design_1_c_counter_binary_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu19eg-ffvd1760-3-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_c_counter_binary_0_1,c_counter_binary_v12_0_12,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_counter_binary_v12_0_12,Vivado 2018.3" *) 
(* NotValidForBitStream *)
module design_1_c_counter_binary_0_1
   (CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:thresh0_intf:l_intf:load_intf:up_intf:sinit_intf:sset_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.0, CLK_DOMAIN design_1_clk_wiz_0_1_clk_out1, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 32} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type immediate dependency {} format bool minimum {} maximum {}} value false}}}} DATA_WIDTH 32}" *) output [31:0]Q;

  wire CLK;
  wire [31:0]Q;
  wire NLW_U0_THRESH0_UNCONNECTED;

  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "zynquplus" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_counter_binary_0_1_c_counter_binary_v12_0_12 U0
       (.CE(1'b1),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_U0_THRESH0_UNCONNECTED),
        .UP(1'b1));
endmodule

(* C_AINIT_VAL = "0" *) (* C_CE_OVERRIDES_SYNC = "0" *) (* C_COUNT_BY = "1" *) 
(* C_COUNT_MODE = "0" *) (* C_COUNT_TO = "1" *) (* C_FB_LATENCY = "0" *) 
(* C_HAS_CE = "0" *) (* C_HAS_LOAD = "0" *) (* C_HAS_SCLR = "0" *) 
(* C_HAS_SINIT = "0" *) (* C_HAS_SSET = "0" *) (* C_HAS_THRESH0 = "0" *) 
(* C_IMPLEMENTATION = "0" *) (* C_LATENCY = "1" *) (* C_LOAD_LOW = "0" *) 
(* C_RESTRICT_COUNT = "0" *) (* C_SCLR_OVERRIDES_SSET = "1" *) (* C_SINIT_VAL = "0" *) 
(* C_THRESH0_VALUE = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "32" *) 
(* C_XDEVICEFAMILY = "zynquplus" *) (* ORIG_REF_NAME = "c_counter_binary_v12_0_12" *) (* downgradeipidentifiedwarnings = "yes" *) 
module design_1_c_counter_binary_0_1_c_counter_binary_v12_0_12
   (CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    UP,
    LOAD,
    L,
    THRESH0,
    Q);
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  input UP;
  input LOAD;
  input [31:0]L;
  output THRESH0;
  output [31:0]Q;

  wire \<const1> ;
  wire CLK;
  wire [31:0]Q;
  wire NLW_i_synth_THRESH0_UNCONNECTED;

  assign THRESH0 = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  (* C_AINIT_VAL = "0" *) 
  (* C_CE_OVERRIDES_SYNC = "0" *) 
  (* C_FB_LATENCY = "0" *) 
  (* C_HAS_CE = "0" *) 
  (* C_HAS_SCLR = "0" *) 
  (* C_HAS_SINIT = "0" *) 
  (* C_HAS_SSET = "0" *) 
  (* C_IMPLEMENTATION = "0" *) 
  (* C_SCLR_OVERRIDES_SSET = "1" *) 
  (* C_SINIT_VAL = "0" *) 
  (* C_VERBOSITY = "0" *) 
  (* C_WIDTH = "32" *) 
  (* C_XDEVICEFAMILY = "zynquplus" *) 
  (* c_count_by = "1" *) 
  (* c_count_mode = "0" *) 
  (* c_count_to = "1" *) 
  (* c_has_load = "0" *) 
  (* c_has_thresh0 = "0" *) 
  (* c_latency = "1" *) 
  (* c_load_low = "0" *) 
  (* c_restrict_count = "0" *) 
  (* c_thresh0_value = "1" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  design_1_c_counter_binary_0_1_c_counter_binary_v12_0_12_viv i_synth
       (.CE(1'b0),
        .CLK(CLK),
        .L({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .LOAD(1'b0),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0),
        .THRESH0(NLW_i_synth_THRESH0_UNCONNECTED),
        .UP(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2015"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
INaBf8vh5mCmDzf2yp77pxZAxQdyEQiT/vG2dEgvrFjseUnGc6ldwH4JvdnpZSpdf/ihioPyMNjl
u6ooyzv5TA==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
S5XIZZtuFR/MZffuhwdnvE3H9oRWM4uXoaGZTa/Dyk62O+Wa0v41pjmZELCiR7uodZPFQfykZ6K9
2ZDMu8dB3afQRMs5lnd/53M1b9ke+MNEeZ/wzjUcsJghubnEAwzdWeW/0tlqST1WD9B/KCxYqwH5
Gj6IZTTFHAXcaVhnCT8=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
CM6IcdzP0PbD6yMSqylmi4JE2qpmxiNeI+prjGwJiD8e3Xsynu3PbGKJAOpOxtR1hT/3mpBcx1Rz
Fkz0QBh4wtE8fiziv1i+xi8T6cqC8ClamjrpZ//sn6dh7NvwSYik14MlwVuei4DZoZJZF63aoPUn
RXkQ13wtK+MkYKBcPVSZMFZmaCU6jMMBYclXzvRG1JqqZoa7mWFTeFZePUTXG7Wo12QaZ8GUi0AV
UIshoN25yn5e2Xr3FyuEtm5AvsZb+iLsgLeHBtKBnsVaHQphicgqwgwv6MQQF6ZNBgU/aACfibDS
3+n/mMMm8k1cj2bW6VCi7a+c8LmCf81NlJuLww==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
ehl0CusS7+JNGq6HfhyaBMy68nccIdIGqixoEztEZfkCpXuUYsdqw6G9MIJdWdu0Ck2acV7K6IVg
rzb8/bNaDDVWp48kupToegTkOdwDkCejEqppido4BkJ+iEkjPniz+aJHlOlOwmauETy2hCMuuC57
oWDprzGWlsqbCjqzKrXmPYm6fNdcOa2DiOYstQaMFNbPU2ccrbLJAiYMHNDqtPNqWxKBsD67kiGf
2eOneDOmdmy7YkNsL+cx8MJc3BVUsYBrpAEsGyFMkmX8a8nYz8R/wlFQFGQAd/t5XrfxFNI58mj1
AHXbcAMhGKVq9YdKeU/vSXY/NwMqp12xJ1nUaw==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2017_05", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
h/qRAwiPuqY/Zg/QWqbaYm8xWTi9SshYuPzyL0UME9ZDDF+C2CyGAugh9HzMdD0kZmT94TKmBgLR
dKP28nlE8VCCU5rvbjKxfn/wNtNKHCvZ1hns8CF7+pGuelhxGvXNmYKFw5co8+4grYFaDXeoZZR6
S5sjvhqtSVD3+qq4vYWRjT2Y/yes7L9dRsLq2D3iZ4xjgVHuIbOQLT/EUKW+9iYudT9Uy6YTwB+5
mSb0QK3YfZdGwZyXB4S3mdF9vNQHdW/rnACq3yngF+lprNkh3ooQKdGqtxtz8KSQxNZOAFE+koOw
h00o7AKpvDAp3uNguLvnNJH3rugOhh95b8Jatw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
TsA04vIYHDZne2CBj5bWCBFH4MtNoFDCn/3DNEi0BwutuUf+X+lD9kAO3kl352WHjQbF79Ssm+PT
fCYpODgWdxSVbzaHFpITxCQ4HcIJhUeW5PC5tw09Tand68D6eg84qRguH+llbb5jdGJkJeTCf+Mx
pupkkLiDvNyTYWe+nqw=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
rx9hgQkvaJJTJJcTjGFW1DrrWiT+xanrcMvFn0Z3KRXlZvf+SE7IQgGCiP7ZDA6T5z1Zv5nzS4h5
cVi+CvwC9UMZRWmLDAjzASJ2nx1g9BjbYe2vHAUmyurIiR6LSigTeM/9TlMv+fFwJbqwuH6FJ3/z
Vl4tIMk4NrqkMn/riOG87SjhesepM6kcQOBgDGzLTG14z3qeZG8OPzxgApfyubmX4qdD1oTgGm2u
Q4mQfFxEye6Jqkn4Rzjhifs/ieNYomHlK7R2/72QJj5j0WyYBIhvO+09izz299Z54ZP2ZXaRMfDT
lU4lQNqQU14PX9Yk9p7sy2PnK4vTwwF0CFIgSQ==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
DVWBXxMRGDJnj62DudUePe696tTNnoHibSY+IaNjC70QX468Tm4Sa3YQdYRkkMXQtHaBY1pEMT59
8uo+HxTZyb6WAyDvTJBx1o1iLDI64qHNWwWOH+ZNQ4chlDqGW0qUlckW7+veLxdThWudN6Qdj507
YmBjjjW4pTGFThbAxraeNSUf3f1fzc1+LPdqJluKhCfvcawM9JsfCpgEDFN9wP0MWKHPYqy+m+po
Ec02a4dF+tZrNZo/clTOfxVp1DASVIvflXuT/YcYZiQBNWxPYvRBSn2mi0nAe5J7a+THqO0rXpeb
ZNIS0CkHs5fUoq3VZ+98IoMcoQZIstA2bXy6iQ==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
bP2W1HC6sTAWrHDbG3ovPk7DwzKj81vYeGaCCjjvaL0TpDwI9KlHxgYIr8AdAG+Lg9Aw7Ad+g/A+
2WvSjTO4mHO0XtWU/tfQcNlOpZ4mdFJJT9Zh8D8o4u2lW6uVL7mxJ2VfRXp0miNPoYZmLp6MjjcN
+Dz9GE7WrFlCI+ZTlTDaaltSoq/Fcl3GGMNr3fVPJ5vxU6yWhPr5uCuZx45QcDb0nNKBYKJAWk8k
ty1MEpBS39HOW70oCiannSpxwt/M1lg8aL3BhNghvuaOhDiVgknYqjheBeu/nnKHcrgcilkUaQrq
iNUiJUcbyun6oK+R59IUaJm+eSjYV7OeEFT+iQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16464)
`pragma protect data_block
kC9ZurrT5ccXMnp9JZD3qRlBrhM2wWAfCOxYMuozJDsSIoEfzYNJimpQKvTubUPcoTzz1H6xd2GL
UfC0jaDCjq5CcY3UwyBtjIqDgbJF1oS9xHmJtmCgtZRkbvT0+aIgAUu3UaokM3NFtuon8HuOYOr7
8DckPtI/P35fK0MEtryt1sfMI0BQT72X4piRlagWXc9NMXJCbdeKkj3aKOa6mVBrRYy3EHTFwzrF
LzdoNeYxO1e4DkqlgmgaeDSRk3eYQPQ0+YXI9J06l1wWyT0ZS+bZPHni7D8vyI3BcMKeK4laze5t
+IfGV5YS+0bVzw+ped3q77OYYm/CJjvORMNYHoZTqNORxmZtlPmmDO0FhFhnoUWYeQPUrhaKsJCX
6M2PZroKXn2OANoSmyxBW4Oxb50hTeBbcF9/JG3BVSbipDoRTaSBybl+eAHET/h0svXbKWLiJMht
6HPoUE3Q4lFtBOhPHGl8IPql2uIgwcMPNEN98cY3p4k7GbmOAIg9Er8WQYcPnkKsd1WvM3GAxTYy
y3UbgILFi7nxI3tzQ9OtkONElKbAkA/IuVEpsH5fM2VIgXWSol143wMsButbxfHIoXuthGQ414CF
H5zOitZFY0vx1ZAH9R+hbVUrhZ8sf+lC8k7pUAUNKlXIWWMR4qWudFMB5lNl7/RUlK88NREUm8MQ
l/3CAULjokNYviyPFiTOZImpalA+uRRRVE1Zfwr7A0wm/CTV3UTRMDfTxmYlWQi1o72lXu30+3r7
qdCuUmAmimeHZFZYAct7UDMxvWmephtU6J3lMP7f3/lf6VpjZcRnZBoTGpW8T537lgVUkp5m/Ep4
PsI7wRmpBYT85m28ALmbE0PcY7PQmNDk7wbDHEiK8em4p5ptK37LrtCDPJu2M6MqpVdfoyjVd3X5
02Pp2UntNzehJOx54xcGhcnSmUzhta1hvgkxxN0l+YdJUZc9uaoVi9kmXkjbm7r0nn5FMyqV4HCx
KrUUoqz29YBbIVTv7srUEAEyCOJ0/5sBEoAJ9tImaXcHTVwXgqz1P3CRn9SZvnFlJvm1UhJfvufR
zh7xU+KgOwWqU5Intm8KbEhhgZt7MV2AlQz6QPVJdUWFCp2rwFcloAI75rjqV70e/wVxc7fuBbJu
BcxwW/gXCDyonAnq/4WGUKjrXuazrv2OV1zyUKBBtQiiPqrPiNT3cl2V8KUMGZ1g2Cr72kqMvrdk
fl8ggOSvzomGphMBwSQFUaMy3vkpI3cikS4lVG99Bq4m6BPEqIlUUt6LqiObYkdO3RsFYVSvE3L8
nh7esEnCKn1ejTsw6Rtk8kA/GWZYuO0hH/2/wjQGtQ3n+V0q6AL60M+riST4CMSBPzSs/BWycKst
LYG4BzDGoFNFUr3HF6vqR5sCZZaJ5l2CZ2pQbK4O7dwXqjoQBcUxwzGG678eS1DDVNQg4AhuASQA
zLKxyuJUNyTcjK20ta0rE16b42n1ZEo00xvaKmY5wtzg3UrQWAFmZlh4y1qnHXpMscCZxoCTvuNw
ri50d4IAiLtfjfSBtcBXho7sP3JlPJjY4JB9aEgOS1rP7RDuXnpCqIDiQdu8papMONB/CZp1atv/
/GuyKHbggrw1Tw+TJ7wM9BTaOoA15dInPlCwH/cqVO2oC0GCLKCtt9kpwlJ3kmodYxhLqFjSYhsk
/14ILt48AuHv1rFSi7RswdkfdDZHGYvgnrpSjBxDBzsjeYSacCnG6hKP48VQQpmKeXSgTMAGx3h7
0TmU4QQUzFcOWWHGyU2G9UKotIStJuVxJA3je652p+o4BxKxELdJ/zgsGWgt6W1sNCvrM5qQ7CED
sSQfWfZJQ9p0sPQnWU7cHpcscLb017x6M0bfxrg11l23NSw6gviIN0MeW7w7keFUCKCD5l/vAfNA
qQ8lnEXzD0tDZ2i2utPCkcyXn1NuoAEyvqVgbwpjKy6fb3KtYJ4IsJ/xeHv/Adk8qy9dmUdNira4
y8zhlp2b1ZfDvNfGXdpAf3JXpoZpgbe66bt490xKCXxXUhaHx8Bt9Zonm8Qg9ZxCDEMQ+0wA2O6R
DGImPN4WCOwwW9vaabtWSphrflpyKO2/xvXiadrN8u+X7F/l34IFBsaR9AoSLdRI6sm3VjB0kQNK
twIifXuVFFml517Fqb7/SyMy3V6rAtP+VDHpHjzSeWly4nXCkluEA3Vl27wpZ5FuhpM1LyT3ZF6s
9tgYDf66IXrYYys2Gsj+RY0c1sCyFXBo1dgkOp+481fGaQRSdz7xPLqqg9kqXqoqnXEf44giYFJA
I8VjY1ouquzWdFXIAJ42xxRBPUwc59VsIlF+Az5Z96S1oxWcHTx4KiWNhRLZUEp6DvrmGgIV28bq
uyjP1vRxRpLfZWLojZXIA/ILcNq3m5IZN3aUqOHMYjWQYqHm8opJ9KU6pK4HWmAJ5xXN7KazP8+s
bBXBlKvfebB2ZensuVwyrQILZqNYNgJpZfjAqpe4X/CFihfanXWF929izx/dZa486p20Se3jNJxh
SYTcu0ML+a4GB0nYFfXOtOob0hS7+5BRgtXx2Wu10OEXUtQ8PrKcO11vQgeuUlvwXRD68JY6+gzy
fqo2kihU0HoH+GsAAE+oNQDJPypfnY3AnxTu9yxT/4JaB+GLZlwKbj2pMxsOpo5ywe1HrEEgxVfi
AOmGEY4IwvfCSdYASr9l5tFC4PuCejjxzPiKnYg2u3pCOraChlKYafucLUqJeCvJjAO88k6CsdG6
P098s87Tkv0jc2WSsqOTFaW7uWQEEMW0u3eYrKFGTHw2VtUiDyIXbYq/ItcF3t4anfvfk/DApqzg
VoO1wwL1WUQO1ZNRNZWW+Y4hVpooxBDF+R1anVtomFxwHJlnX3D7Mb7exI7UV0cyitM6taN+SZMP
ogdbzWbXW6YQLfePogSeKWz3DNT6f7Pf4vDBpuMpqFoXBOakWYqV6ZE61a4x78Q0SyS8zHy5zCiu
18h+kyRZfLy8ZHdsTmbdPYTaTJVN3bfBe0xfi9aWgMpJD1KdvrvwsfBlOryFhkJRk5xEuSyRbS3h
slVsFKE/OubJ1BUwF8csFWfQhxuGnRyxGYTp/CIYTSHKqQES76BpIN+gu+N7LgYROz8aiarb1Jm5
sVZHE61FywYFyb3l1H8i4oSMQgAk+FPcNbt3aHGqUsL/dli1m3/2qXCQeEzo39e7iFWBZf+hS3Ui
rtkzf9Ql0wT/4WBwE1ZevhMwEgvFpsPP5ePqeW8boF1bH9pwAKBN4LLZFbW3arfnfiYZzqSSPqiN
ntzsrS6/AJHVX0VXZ4XBkNiIg6trjyXy1OeXxJp0TlKu5OIJk1YsJX4dSFSA8Ueprty0TkDP0dOO
XK7ILQ4VP3Bst9Oz6xahXIn2kjd7aenaa3of7pa6/MPLGI9GmFfS+TZ2jguhDVdcQDRtgl8SJ/fp
76djUrzsBu8CZIdoJ5ARkmP6IwxKtYDl22zcQoiWiJru/qWNQ75Zbuk4u3UHP7oLTzFLKn0fxbQ/
4HL4cSD1N1CEdSTsZxJfYMWclZFGO4Nw9sNiNgWKuxAXLpSTAS40xgwgGRo9x77nqkhw5QWtwpHY
WfQ3f2dvo1ASn/2+ijH2OeYX1IduujB9FkneK+M4f796NZ1uTiDm4B9E4vrfmV+fBnyec5TGpHBf
R2o6yo+FcvfWxByOlgt2O2YWVLHlckRzWbJ9/Gwrz7Rkz9w0Kz/SIMhbH4mpPq15rRs/M4jpuah6
KQIdZXcj6PFuPDvFznXd0ott7kbn7JIoSLBd5WKbUZg4CQkHEDoqEiAo18ok/ZrGp/ELksO+3Sn7
WIzI3MrWE/0UnSf9kWldLIAIwnw2vTZ6ADMRAYr6rdPWJxx6VTrIGxy4/GG6fopvN0ozbD+K5ygt
dghkzQt2hP2xy0mq03S3s6gzEB5ZaGWcuX71NMj+CR4kqA/KbOsZMOJwucfjM4sNlvjIVDbw2W6Q
wxcP6VVhDVmSRqdk9VH/0PN2/iEp/53Nv966jXkc7GZjyEl93MB+Bz5reYIt2vpkwZngJzPuewVL
XhawNa/kkGSLv3wTFT4+6je+/OUGi5CRUgt/Z7MI8Bfi+WqUBDmKEKPb6J2xBHoP8BF++VNHqqTg
Xx4YUPJviad/IQQ+tTxvS/Mn0u9lH/RhjTjAfPlcKjw7PdS/VaOPJLf4WeT/7IqLXTmr13nq1QBt
Wzrh8tGX3X4kg1BndtDpwJalAHBBRbgwQiFhpHsEtb0gbMMnjRDixfZ65DLb2BcVjB9KwkjcV8ow
88XW/GRghCPS+PkXrI5wA0WVE4m+fioeiImyWTBCiKXGGvEoReqSWEpMlE6fgxnoraK2IwESQCOy
C3WqcGm2148MNjV6H0JBm2eTuAeWaW62Z67hW5ndTVRNc0GWilpv0hhkp0H1lWCRWY3TMopFcGbm
IvVf00R6+WD0TiGXPD2dQs93fcVtgj9Jggn8Xt+NFJOPowvC34gPljKoJ9xTNj8mgV/3RsAjyvOI
1Z4REjG7uUWZZO0uYeOEsGVMyKscac1A+iS8mtPNuGVxT+vd7lSnf+dphxcbJ4twCikh7RksR3/B
c33Xsstcyr/Tx3pEAIqJpk2zoo2sds//Xp5rQBZTvM1nUntv1Ma5TOeEahCygvxyZZ31I7MUBqqc
lP87N7RaTSGTKMNc9LhB/nQoyqAB6DTvPXEoFzEa1tK95nnxYIGB23XS685lOCaH+7O63n039DLG
WdOJSC88/e/flTX+6BbaJOIih1j/iCQonpCavOFAT3jJKhMnwF3oSbk/WSdvxGTq/XxcbUtVPxSV
UeVNRF0K27tc6605IzSypn0tlhytID7nYiukDrf5jCA345ktqnxXXRq2zZbtF7ux1ooymRF0Zy5i
VCAAKkqOKkYR+IFJZueFPpptfI7gRRGbHLeAWXZLX+JehIiW7h26yMJhzFmT+TIT5qHb8qPP9naW
J8GzmADarkiNxa6XLLGESwyHcRdHCk0oYzITDshPDW2IL98OboaaQ8Iy4bbv4FVqMdGRs7+vhoYk
ZjWi5VFkE5c9BzeY5syxhm9A9UkjIpmXmvGexCFd/2/lkRqF4Ie/VSzAd8iAlGjUW0gjQ4a2xfuQ
i7n7A21YUSs8YoyCMqwI5//Uw7tJM0Fo9A5X78LKz5ugAGGMO1/Eg8uFKrsF0L8r0VEC0qJ6WY/x
VN9IuFjlsCUVjxBAg5gMcaEIGeGygTyaWW9sdd2FMj1MP1BffsydmVt6mkKyOr7n1mXBUBL8d7QC
MCmXbEeJ+BnnNKU5CFm8XS0/nHfztt4bqeK2JMa09E2r4xgA0UseYxZ6QyXuaCXxfEvu1Q09wUFr
eMFOADfnJQiZ6OhD3GG5aLXwRVjuqDCmq8VVQoa4ZjyD/Vv2uap5jJqQ3pkj7NSfKlfZOGdq/0Vz
/nux10C9STVC4x4PVcFBiX9nvNNk5HNU0kzolcdXbYoIHH9REMX3HEcaKvUSlZ9QcUfMkl47ddyr
ZVOrrpjGL1fxE4AZZB78g8J9WDr9P5ece7kDrxfqLSIDPhDPtQ785q4gEBq1HSENYg2Wp9G6tLcd
zJT22GSoOen884By1YSIrayMbFEl4gbs807WLhhWTzM/jedPiQ5DQbakkeOo6VqtBIv5tnqtd9tn
otl8z4QGcf5W9aujJZ6YuN3i309oP7cCysu8B5gtJ9xWOzshOBnF+rA6xe6stdetfrHZf9icEy94
2SS/OuKOCCTUC8YgJIWU/KkeOWgcRwZJYRLmG0a9x8eNI4jnSCn92R5+I3QY6VmRn83DuoZCjfOi
rweLWth/vbhImbPkbTCuY+ukNc+2UvgKQ5A1LpBru2dQyawf2mNWbQvpwCD8s4pRan08ZwjhX/rK
jMjf4RW86aESS/uvoqcNaXsjfXU9L4YO4bsJkiMEfwntLUqWmIstehWysq1Dq0Ni2vw0WLBLtki8
wziFMEykif6r/2vbzQSDuRl88KNfbMyF7GroOo2pC6DsJZ/z3EAR7marOFs9SHpfchqWtLjIrza4
LBf+QIuSXFVf1r2ymRlF/gz7lA6PFaelW0YSdEJbbm/mUpNMPAwL4eIfvuVyyeBBGk06MLOSZnJo
mLWmHrS7/z/NloXJ/Fsryl/l8VTRr1/TTcNxs5Sk3yZKRphnm0nfvTy8H/akejbH6qN88nFC6FnC
1qMk+/vfH5KYAXN99Ps73d/J4BBl+558OvaQCi5EQv3zCoCeGHx8Bpl/MxO/opThcyFh0OKHPdUA
Xy+y/RTlcSgVCUbC6sJL0M4gTY25h4srF00cyZSooPDYKvM90p4WC8XzMG9bxOG71CE/bPkEyQYp
/jiA/DEMS2giv6GWht9rhu4LHgHfcet11HICqo5YEcwkbCNZ3B3i38WjeDLci5K6oXJQO32TSz1e
UDkzf8QPSYthq2NbxI+duCg5NhSlCDioXGCM5KTADV3BSIxTpXZ8f7bqKLBwp0dG/MoWnmSic4U3
xheXYskvWQA42HaLpRCLrZK6q320BUKaYI29oOZTxaGc67GCR1eR8gN5CxJnrPwT+nu3U9P6o+2J
Sy8B26VU9juDwC4wxC7nJVqrhb4BjTycCQ7C3IOjFKqPCTETv9qcpNlnJWxc9+tRnIq38NH+2jsB
NE2Di3F2C+X7FH3lH9xV6UqrkW27RTllmWuxYjxb05ewyKJZAjHvzlNnVn/UiaOC+c8L/6sEDcjb
hWso08n103e6RclIgJdTwcgrlfSWN3mko2YjfeH0Jhr5P/BCqX7M2O5h2c4sNFW2F8jN1qKwALyh
hgwNBufs2b281ERXrn23APY+M6nb/5QoDjOe+HySa+kmoEzPo4HN3fgDcHskfK/9I7uP55J5hmO2
WuNvoSnj/x0+qgXx7bSteY2uzrMF9lyWh83qsZYaIggftrfX2bYJsPCgyb6xVursS2oLB818YlcZ
ic/LYv44x64lCRaC/GPy7hVxKHwqnbWZDJHrfkH1pn/An4QdE+roAd6Dh0fua4dW4rSnG7OHHao5
FYU8E0EKLADYfKm2OONDZPHqFSnGFjnhpptaTtES8/+nDrLsHdXR9touZhZTK/NQL0k/jEOxAGGJ
3S4ucIgnlGun1JVV/hC8Gmw5vopC89y+n65LKCZSBRWTmaHsWMakOntJS7brZutNTFayEEbNbtJi
FzsONz5GZSHMvqiDJulXJSUDJ7R+3Gsi4bGUABdnpk8W2Eg28RmSGhR4ME3FsjG9lkntvAxt4BJ8
0vES+kPneFeSFswNk2TkpSlYcivP56uAfupHvftb4WYxmn7S2kEUVbXIRyG8EoQ7n4HmXs0E6nzq
PKOH7X1LA5mJhxr3znbzNKfuYz/mcVeYSGEcOjq5//VplQlETOnNCDpnjOZie7QargKMGsM0RoTC
5zr4eINx+NNvWadg3ALBvzRNNtSyvxIp2vR5YYQ3XDhVHlVYG4er0PMD4HRkc7wik0zW4WeQCPYW
LN2RxmqNg0oDEmnAJpjo5gS+6Y4+gOcwvEmGsis+RfWc39vxGzxJ6o8dpQIyY6XS/LbD7QixBqVG
xOrKVIeMqsjTUelyXRxduZz2aNQpUPYxZa+5t9/oKjeJCOH18lzY4+BXSBUxpp0GtrdLn27gUoNS
s4ptwVSSVKeEPn6CpP7jdXVi42Ehg3xc37JkzuOijNDZ2WAfjlquZpLCtLbhYHj9hq0HPqE/SMuc
l5UVvc0QeSfdKQ58hhW9nfwVgKxI8JKiRfHN+IiOr3mCiAzMfTfkFboCulHvh75mHdfKVJUEWmwp
j7k1pT0YEuSr6deOgrUtKBq22/Z2EeFZcreQLNdH0WkAgsIdWfY/kggqKXbJ7U+kLSNnuoKGz0Ng
EdE2vOq0CZ+oiZlTIQIos0GX0k9WiBx1J1TQhlX+SS+MRtYaHjMMmUXte6ibZ2qCmQMLMLTZI50P
KaAhK4Hx5BIRIpY7PHxhSZb4XyZFYCsm0FDYZzqAiuCHBE9MzvJfePA7g6tkIQr6dR2+iFFuCE9I
YrJvzgPUysil/RL20ZoHPOJ9p9irwFWI/OW4MNFEPWoGskSYa1nSSs07DJfL2OiMiq+Tx/PF8sho
b9WLYEbRztDJ5hGfty4Y/XFejf1/kByrWEHkNoORceNFKDn0AL765f6hgERu4hhiA4jUORu3WIyw
KX1nZB953AbJSuIc0EIgkQZdWEi0NMr4KSc4RGmUXT0KyrTYa5TkdNSGd5F3rSKs7SWjL5y0y0S1
Hld3zneHDTpK88k32L8ndFdm18AT3XH1R8q/UjftsX6FLvc7tYOlH4JyxxR0MS9rWDt3KxXZQm5K
pXqlnvpWYwMDv0tyuq83HFTSUuzQy4v6dODdKWJ1+NKMea+C/cdotbiNWHZ9W/eLEDXLvPHIfEes
QrBjmjjOYglW5TPBkqM1BUXgA6fr5z9PkpRY7/QmtxoA9EG76FGM1egDxo33rVR31oCZ52OpdzuU
ppjUJ/5E+hOT/9RajC4OL1kUjDKLoC7RmOTMW4DFHhJNmZs9BK6ZO/lacX9b8trLSBEmCehRO4o/
65Eplfkze6Vql5MvKaT6tVeOUEN43fgdfYTk1kKGXuNZJNm8EMQT9/WIoVm/3f3Yxh9bvmRQq6xe
v55etVgz+m5YZrV5W5suKMJCM+RFryB44nx2ENx2Lm1hRRMOVo6w2qYuahx9uGxjRGVh2KtkNroN
W4ewc1eW9Y10gUUBsTfn/w2yL70G57HJ/5xdP1g+fnWEFE1OvPK+kHyGttW1QgUH4lgXFJ3owUHM
venlGqYaTQaUf7X9GHSQOqTiYPtsDeli7PmsJzR+JT/v9ZlhgstuZhxmiZX3cDk01dE1+jKobdjt
FYOpqdISSU+ORQKbPdhCDLlGe39WCigNCtHvNflEhD0QFzjZAbXXu2g3FlsfRpPd/y7blNovLd3o
VCOGbHIfrEeqMRQKVBZlGL0rmIpYfyOot4/RgT002RsEfdKh2zX1zvno7fVCzkATuea4UW0Jzp3s
k6TOw93pK9tgrkALaM/JEBRSZQ/SprtloFbalDJK2WE+1EB128MsRmUS9ZJvGWaaSb2hgRPv3GvW
aKyixHIrvWzRP90x8Z1d3tMlrUjcb02wVKsmncad8AbeTtV8ZFPBdyZQfwoSuv7aHtCDpf+aNQ01
JqSE8EjLq9utKgj1AZri2KG6Hn6JAOAClUhrdwfFz9s7V4O7TEt2UN5rAxojGv32bIQCwoaAf5Bt
Cg/y0+0uL80biE1AOpvrxOnAvBqLQgJu6H7fsiNvNOhziOU5Iph9B3YM0Zk2wKKH2+x1OI0jYTJB
2huaHQHY0eJMN/0xAMlpJ10QaTEHr4OryFAKrK0+sWbg6pOFNt2g18WQCPAIS8NrqxjGtx17q5O/
iBLZZz4sYXSsV7QhDkk1Kwb2GMMljOqzL/CNYVf3/Yp+BPbEQzEnbaA+U1tnHFBfcO7ja51bIF5y
skXj2TZJy5/yjjzuBINqTpgX00ZaMNAa8ynq/ipKJIMs8PTDyKgWaOrM/C75nh4wb1yQxIf+S44C
OXoiQ6m2D0iMtIoWjI0/ZGQASb+BGet6h3whS16guA2AC87cacM7d1I2m9e5ryxUUNaQhiQ/21E1
ZyrvchfrRdocRkdYqFcOgj0KAJSEVc8TBGft4+BtULZkx6Scubr3pcPySNJ+5GrD2/JqnjrA0O2m
Y+NWwtWLpNtRK1brdODIGX8Fzckwq0YKO+pZcW8sUsbHj9YF9MIDgvpfNt43iQNthZZL0Ax7Rh9c
M2+DXJFQt8cbn2V8T5tFJqu6MkrrEtWzn0vRjRrDCpXlVIQd9n9kXWyE3rkck0/dQF0xhJ5l4Il+
zqXkPNiX4ag4LqW1znpy1wIqIv3WIwsyYfyf2C8cRuwQQw90FpxOZbwbFGyFEW26XxZSe/77z5gv
rx6h0/jwMjTkFWiLHIdfuD4RKZbhG0P0Iuhnjfu2v9aJGY021/T+MTRcZzVB8tUA3vLBq5eL2EPT
kdc+NCpRLfomxdK/uWS2Dw2Id9W7BY7IR/p4UYI8Mxz1PIQTusSypX0BkXh8d4CpPuCKyJvIrO58
YtOuOwRAQj6ayxJ2Et9ON0yuZuezDXsCTfJGl3cqC0JdIswdKz1kG9mGDa5YzHfnftK3Ws3NkIzp
ttAqOJpDRLppH7V8NRiLIJMmzlU11TMjWIaqcmtWCwGAbW3SDJ4FnFSHEfh01OULVzJSTqo8wmaY
uE/yEQEkR10+dcuLNbXN/V6OcSMl/LiLF73D9ScsppOf5PqaHCWYiL1LwdQcAR3fAgiAkb2rqWOx
Tw/nRFTzXi443vTXVvnNrrdHwRU6QmPB29If0tAol7Nm6v+4yAFwmnLDLrm7Zsmrh8qf19DIaY6t
AqoRO72mZsWSR1NB2k0TKro/RH+Pjrl8nk2pH6h72aTBkwi9APapOk1wIPtCUCRSoNm6r7UQ18iA
LZ/6Socoxmk3NeuVa78htsm2Tdy9umtMPdlVG3PBnkitpMAJkjGPKYAPDq79us6qRkovqW+qx+gA
cN3/LXI2pXgf/Em9SE8+0f8erg7Z8kcBOqJMMgmFh39UEv3sNsMDQzSb9N8gpuour4N4czkTvKI4
EGnCk8yrnsUZnzYR5suC5ZExJQjDKDM+wPNGSCMEaTPGu/SboMSs9MmVT4dF43G5MTLCDOoRP6ib
FYHOXvvPTvSkKFt9AF8bw/4VW1fDaiFUdnK6GLRIAri1z7ZeLuN135GNQ0oOY3rtXrESYCgz6XF3
P2eSPNH+UuOyYPPbJ8cEV+MN6Y6CEWf1bqCmMss7QcZIu3dCNAKJ09gsnIqJu46XkAFvSwBfgcjB
n7HgOy8UeyMDmWcw17+E+phaKK2Ebv8CdEidXQ7sHNvEhh27Loxuhvvjvb8qJAmZ6I720dc5zl3r
JoLH7xFZjbOZaJJ9lAqUYUhudRsCeTvavc5kJ2RuijSnw8Eibf0cjFGLv7pG5xjqhJB6dcuFy0sq
M5dQTzK5W5iLtSF9wqgwKE+4mbQJspaPIShofFM53LJIRTm7C7yPT915XBZgRn2skyqOeudtjSrV
U1t1+WU6sJMFR5d48164D+KL7xkO/QWAmRhUeyNyfKUgeV0fMOltzf3l2+/pwwlNdM9yK50mW27n
L0h7CnXrH5IFHos542sP9ohPSuN+xBQNWIVMuwbMJSuz8BQe7teIkt0QTwDd9N6Ea+Pg/5nurytr
U+FC3rlLd9FmBZuFHLycHQfYTG9j6nUrDu6as5vhOlRffiKkaW4PGD8D8kYhRzRlX3T/0vgGrZac
2lnq1/I3NbJxJ3Ffb7wocImYfSml8zJqzpB7O9rfwabPCwr+sD/KAZQ/avjMGg2YHI9AMH57Yln4
r7RLeVWDFzSo6zh7dftSdkIV+cLeKI+fR4Au6AkzyscJP1AhWKA0D8JMzBIqLfg4ip+04xU8+61w
Zzs4jlpTMlDfe/RNmZfULZVN/PJbAhsgaKggwv/L3Wrppk50fVeR07W/Y1hcO3D31XazluvKJNx8
fHs9aH1B3tsouOv1z4pandJCIXEd7Euo60/KfyZiiAjKOf9N8BTru4ImjbvbAZI+bInYG+I7Drdg
MjI1RM2rjO5qaSLpzQvVqXLPiqBw+bqeYT8cRtR6Cx4pqWIG3tITqjLTairxSwTCHP78JG38Mhhy
T5eROcjy+P5BqijJ4aSAuH5ZMaHgRR8jE5YY6eICuEqm4jy9CapCyrMZ/pxUvTYgUmfBu8A9yCoH
gE4uJld5rL01q5FaBAx0JSIkY2jcMBUJkhgc/mpQNCt4h8Fn2rc6WII/3ZM1q+aMgUbke6chb8Ap
pfjTIFCuM4RbRlYPwfOsxnjOhsHihNOKXceQS17RE/KCX3ZD8xcl2vQTxqgF9BeHOrv+hqZ6lvkl
b0fyX3sMyqOfCqN+uAlqdVYQawD25mfluO4iklEZem9E+vHoxR/KU4m7IS9IO09Y/57SswPU40H/
jw95O3F0tC9xFN5McQcDRo9/COt6+ziXxZEGAPZuLlGNyTNRADeuw8yvaZVNihLtI2O3I6uyS145
+bSj8Hju80ptsFZUrBkh0Hk5ifl5ydMCl7pTXtQb4In7Mg/67UrtS+SyCmLGToDhcgR4krU6Nfg5
rwQ0W/pl9miOVKac+GIFXTIz03WjTcq1J3htKu6jWi2+OEi7M6ciQqgdKAvoUuQcAB0ySINDi0gB
/36xb4Sq1O2WSR0jWMpTXTwdvJ31mbPvQyEMZCgYC+HdFVIz+OCFvyi3MQQrlILLja/oLQVuFZsA
PHb/q91t5hDr49M8XVOV0vOTQgOuS3yIwGqhcmfj4XPf9N1iwciNas4TpCtMm8sbeG4ZzlIh4rbo
KoYD3hGyegAO5Sj6HhqiA739d0Xs76b41JMXQjEo3B/uLQnO5C0RJZI2pPbaRcbmYWelo3ZZ+ZYi
0CidJSXYXKmNwhqwYeglak+cTmrFYTvnu8yxwAzexpQ3GtbjYR65W0tiWXXsXnuKTFMvcOhghBuB
kmljnnbC5H2/fEn52RrWZXKUUiqF3bBP7QWFvmLaIFb5niJGs8dzwufuF7X0ngTO//khRuBmuXZ0
gjKpdeDkEoGQFYo7+xXLL3H5zVRaZqRMcW04SOfa8CScsi24rpd2XUIH8UAblDFN1dJoNCUUt4kN
PcZBK1vfTkkMlp/zds3TgHz//mfEDRqs6ocHk6dC0hKBYzGYBpDn/fsC6W0LXxxPMd1TkFH30Oew
TmZ1UYfDxeyLpgX1aSIAZ5C0eZeZllTN+6jcS47gjhoqYrEkgkWVyB45becwNyuFCvPn54DEWDug
23xXQ44EA4JFHeBiI2o3rh6PSlfd5S8a0pTP0gX+Pa8CUl+TrUJ3niUVfGY76CQj0ryzq37JvPfG
3b8Hy7n76xGd3mSPUl8gla6ci6Es1DwnqtMfBFlNHyUXhiXVausdsbwV0d8Ij+t4wJKMZ+2drs6x
4moK1KQzraCzY2k8/Q5iCTr2EtW2r2oOKe3Jpj1BNSIpCoIsDicJ+gi/rxdnqkuxdOnFzZg+9CYS
nkIC2t2efv13Qa34+onL2tzEEOrqqUCTC14/VhfhCB00RCcpz6ZaZm3watECG3wo5rUATUJ30/aJ
Prf3nYGiEeloBJb/BAhJj25Vu5GnYdBhUlCswT9N3G4pyoYunX4yZ2v/ffkrpVXv1nn+1k/vS2pa
KJCYrdp7E/OW5umHtPHsBn6Dp48hcv2e3qD+BsVv0LORSw+QUzVKRgezHceZkVV9kX1eTKTQMy1g
f4C2dtJbvVfDclCqQXuI4Rw+PLAoylSAD5mMPki78MRyyX2MHpUZE0UQWeKthWmc8W4Nmp9c3zbE
hwr7R4sYYgHLiHhHLBQlB/rn6FBqst+eZd2nq4akdHziTEU9nzOarpdoG0pdVWymymD2AAv/kHdW
gt8VlaD1PIE8Uhv4hXxpavfKiElqoBKO5znZM59/8wHPf9EaO768hJ4SHge75iVuhV5ovV3dS2xi
qN3F/pvkjskXXqZpXC+Gqyp+NflO9Yitdg0yUK15HdROJ/cV4jY3ymZMioXqpqICZ6KBxllofq+h
kY0BfMwFZL+EHHe+tP473L0BJcB2fgXn7xGvYMCHN7o0VL/IimK2WPKdU79pJ6aE1CqreVCNyRjH
JMndYtoJZ9lgq40JJsK8pffDx6iKEJKgjSJ0ziMHIMAe3fPCCIesy/eZW0QvikCEHbS/72dsi6qG
ps1lWnPFZW5UzD3xqEW2YsYZWHObzHjFrIpKvgnqwyyXi94hXcU9mYgU0TKtyPY2DhkMkVpn0Z5S
OSpkctzVKbdJCwVJGBW91uPOG0/FBHkeAwN58f/7UDq7u/6ndWoLG+xTqEchlulA+HM3HILhLpow
8jhKPAISelQ9cd44tLwacRCSv4Os660Mr7SyJ1VWOdU5dx3V6X62kFuBHrMOJ1+WpTf5nNXDqC4Q
6M9vxKup7BTQHxkRDvHRFggqZzsL01Fw5WONRSk+zjpPM3XW2VbGMyLapcWCAaQuH1XXIBAZLqZ1
kWGia/tjSTdqr0F3WThfdTN0oTw70uEIniMBbFYoNcNvpqdyvvXqRv7VXx/RknxexZ+KlVpI7/0Z
leiusYn1UlF7ZGkc4tshbNX469Of+/HDKTG/NxnFZwH+mFeZ38s+tgSwrQdm6rPeredDuisE0zCp
0wFYfgVBbjyZCkTKw4NEGJ4HawOl/eUAoROM51eSTB0qoUMP/YlDFHOTlQfVKh+UoeJsCoBwQju9
ux/Lu8dhaaM3CvzndyKtfdmP1F+KPZ0lMXpomUnoEbJIXu+bnid9sn62zkPrmonVKLx9hsQEG61F
HRjM/gd3PiHiQtem1htUfpl2TZ//bqTJV1/8xfPh54GK3HSQdnw8AIvoXVddi/m4Znlv6a6cpgZG
asP1RB4iwKle5Y/neeb1dquU+DJ4jFCCrJHkALZ0DWkO0lHJPdpt4oyuzqL2LRJn0lSGlDPQA4RG
qO0u0LKDFH7/eIQsV3UDRlCFYkKfN5ak70LgtYtxFXUE/8Kftu7uc6WMYPhnXKLXLfMcn8zz5QDV
Jb4DcBCD9UM0emnI1PRdIISD3qv47afLK1xAQwudG/BzlEggY52PO+QlrMyG43Kcm74fY/ZLsfoG
ob99ozJrI6w4HTL/KRNFEE6RFSp5kcDBNQzz0SoititcyYN344lM4XYVNDveJP7vnD6lM4DQndJH
e/Qtb7763g2O7DrD1TqD2k3HBbrY2eobTS62yg3U/S6+Tt6EGCiegIxvjmG6SmpuOznHb60E+pUW
krnP9zCUn5uvLF8SIzY5od5IKvNnYMMQFDtJ3l07XFIsG6uIgR3lrNkG95mY5yol8UnRmjMeadZS
3Ft/GD7U+QFzokU4ephJkDieQs9HaDkJV/uB/LM85aLeN3Rzk4qWWGv+49toGpgAsc1TRSnxVIxE
M9+9Ysl7lDZ1rHM48Wxop/wtAGiY6gb8JB67zmynR8/priHMsZiCIY+DiX0vnksz2zqtm+/XcdGf
N3ZeDpmG8XdAQjHHARmYPAVQ8uWvA8y+yCOyZtPiqml+EZyVzuZcx74ZWpqZGaNz3b0Nq9Pg0ncR
m8aoOXux/HGmJ4I2opT6OoH+fZil+IIgrhxQALGvksnAbEf8oJ/UTZy+efpZ5KyB7G/LxwtwEo9m
cgm8Ga43hqw1PrZ4oAccMB1LeZacluJzlRFDAlzmHI7q0rYGdcQqI+9id1CBSpd7h9gJEBp/YNZS
0l5IEORUfkDObs13r04UH20bstBSXKtbvS4VFhxQ7Z5Zx1OEk4/S1zAYdlnm6/WbtK/Z/qwk0GBK
2NAxhgu20MapfoHZECO4Sb9CepEDk5c/cLAwX4yswpRBpJMlMY5Qvy3dyMDiaTqNFnM4jgUOJfQd
nFFqw6jQiA9gvjzLb6ZlF4ghKXofXuSjW4IPE6b9ZTaCCDNB5VSgwbufPznHtGKejlPf7+c69zpN
CMoyMcuw6Ea2uKXRxP8eUkN+8Rhz02MMHGVzBxaEGCxvMjsXoFTxRyfMTHFiux07DrTjc5lYZfD6
l3kek4zjISYbTVAa6i4Og0WjilbU0hzBKicjc91+hzaM4NG94CVQomD9r7KIbFrB/cYLRPsUaeAp
trFjJtUMg+GAPpmjmVxG+3pROU7MWZA9K+giPKLKizFAqDSbMQwQrLprcR2c/zXKOZpBJZpOSqja
GRipuAeRY6kcqfcu0vmSEY5SHihPgG100ZKLe3t+5lwRnwT0xR5aN5VzAnTXU7YdS2KSoBU8am0g
GAswtXUOmVdQQyI0QGtv9SFO/PGfmKRPDcwKApwtrIgnp9iemmwsesbXt/eKy5eXZKr66tC+PEYf
RSOR8o9xlLq2WXZRDGY+QNds8TCc+5WtsW3Dc4AeE7DAiYxo8mKDPe8yWT/tKAaNkiEtdzHmYQKs
3w9i8IRRbM2D3ATQBLfAN0d1jWR1wP7kseuO/F82p3p/4nQWeAiGzDSndrTLzXv/AYYr/JV9O8Je
2Be1iM1xPopkrqP0QBeE0rKeCyY4AvsxFk11mamzOeHdcAPAGiPmZgNmWbgejqg+sku32kHBjo66
23yOb86gHM0wddxE1mfzmUedX6A3tvdrwuiGA6VJqIZREZkPuc1Wfq6W7xZ0O6srk/UmS2sGMsx+
jCI5spl4DCVjn3GgwJp2MOQHU8WP4uFfFBYAk5dW4kEhqHkUpK2HnsQY5T3Ea+HLnlK3DeGcf9EF
rlz7Kgt61EWS/QWbvqQAbS+MJDKDuRNwS3XgrBs2i7HAmNOpR9flJwd5juG10otQZtrgtYimIiTM
WtKjxxV7zu4H6dvbeNC8qBoyaO/UFIonv0mH/m9BrAg58CKp5MTUkU0wjkwP8dOMF9HfZu5uY+BD
eJbanNVEOUMF1v3Ewd+CRToziZgye35Piv5+8zloTlJQ1A7EHErj7qj/17btFJT0A4u3eAW6EXbn
KJwvPQz13MGWjnnLAAdpbIxMMtySxeCyHU7nTUqPs+wIe7ba/EgsxU1H9v1UOL9R2bttQwOQ5h0b
PEtkLEjf+/x98IF/sOTQXh1gSQqYjkZ4NrbnmiAg3h2D0C/ycTaxOCSeMVrxilt/jWGk2THEdnqm
l55k94cf5eISoXdT4hxMIlbMhItlqvXGkkeDu+aJArrtNrCuWkLJsoOh/0bjJWpnlW/ZjwX33U2+
yc1GyWWh5QEBw0fnoEyzHavd3eKFJCxaDvzkEBO5YsH4B17Des5tfdyIN/yYE6/nrqgMWgstfslu
j970lsmKTl1gX24qs5bMsSOOvzFRhUBAOmmZIqHIy+NzN5HX4ig2X8kxiV5xePecoVPWLe47tMaf
mfXpBw0mln4w9Cmpn6yJtAtRnwtDkqW4RIwIbHWThQAY8reWA6E3r8MoLYb7kjeg8cMfkedLAlKC
LOzB4gREXeGfUgkSJY3bouqpttWRTbfwf/xn67N0DPZwkshZCp4eyGw1dk8lRqdoh63MkCAXFmyJ
pb7m6C2XeidUxRam6YD2WTbhCQfjavYv2DE6pkEuOtcwKdjcC4f9+YJbp4rQktkqZzQj+ay7cV5T
N7qweoFNbKbwAeJtq4WUPj9AYoQsHOtv/NUZ15/rs8xrQviO9Q4lYqlMOK8rDBKM1uG/gPv+iN1J
YVV3BudamiycaD7lkN3ziok8tAA4ttZ1l1pFTwdqBpmOwTjL/orwlR9fPuQDi6PvoXLQJ1N6Ic9v
cdUCxci5VItc+NekgkWMKjnb3JPnHzIkhZCgbG8TJluZdvftvHE+tpGVkUKHoLtdp8a3DM9f8C4B
i7BMdsPaClwzwNRDzFxul4aBs+NjTKkjO+sS2KhhhwjhOV3W1RQ4WtEKSOLsQGuTcL0w3BMNXnup
JgxrbV2c0S1Mcfkw9es+USi8vCK1jInx2vMzrfkFtFis7q7kwvTSQ8nqmBoo7Udv28v69oj5RKxz
PRpoa6OIsgEz5xvzwwXxiYcJ1rqPEhsyegRK1U/3sD+D7lL90QwmVPy7W3Lr7CWCtra/hLv1nxkP
eevVyUy2FLHQhem7ivplepk9TNBtGc1pOUxqc/N9EnLXPBZQEHgYrGPw2JwyHr3choXVpkGKm3/V
c/z7p35xatwE+QoaH/NZJ4pl4iCbj0jM/Qi2gPG3+jatQuS26vtcUDUtgbBEKWb4Qg4Yqhmlla/E
ghF7zlE0hASrOwQUwZULIyxK55MiejipZN12RZC3q2QU74GQd9E9pD5DDk/3S1O6J7PMxywm4yRZ
4AJocJgJO9iWel1TeLuOE0+dVw3XKtcMRUbTfU8303r4BHfvC9+yX2L2t8Fdsed1lVAK6yOi4QW0
xrprOwxIpSU220MeiiJ0Z6VcWR/4zzTk9ncUF5JBc250Ngkr8dwPGzp4WpFcBkPRChh64lV1JbP3
XA2P2p5ymjP2YwA9PMGuQtgymXXZoek+R2GAhBMNh99IBFrLLuY4YQwvuGt7tZmkgINgQgOyrj4N
cZMEU1hpkNs9seFshVKOgW1U/FDLG3pMl/WmRtaLGUjJ8jmNvA/eJNEIYSfvHGLIIhlMZUZPFWPm
oSg+/LwSFUK5xkyA2xpjublL+ozouDtUaNLb7KB9pZUSM6fgeVX/GTqfaK+yeGntdz71BympmVMw
CEA1VaznKtOR56Z0fuQ/94MgpjQ8IsUmEZvMV928s6hC+WmK+i3m1THk+fasXS1DSUokMg17l0fy
QMmrxPTUx/7VrXYsoOVHtofEDGQ7IpqWInB2fZn4QfqkZ3so0toTQXwmM9nI2W1DZxpwaqPNqo7L
Ns1eeYakQtj6mRqH8Ta4LLEdnsX7q56SpKFp2bI/C1C592GeniadOU+x7+Vlbv/0CNEhhy5NCbtv
qorbLUtlh5sH31eRShx+Z5oFTKvpuLmknuzFObQjaIEMSm8YKBLbHTiHlF85L4kqU41UarPX/1fK
EN5nEDcFt5i8W+Di3TTvGDue7MWFppP/iFieCELJYGIoNL0Eqq919VBJ8qhc27MYMBjyfAltFXi4
2ZxGpR+nx9XOwKY/oRO4XtnOl1oJ8G5ERH+UaWBay2bD6yCvsVwijt1cwSYRiPnbZKFURWf354qS
dCnVfRSXYCY+EdV7iHd8+pBj/XNJBKmyuhsakjd0nx3jtNBYJ9izMUC+oNdNUITWC/2GzSv1ycxT
L/VHJzGYYTKNs3SJFHDvfQspvB3kcWAhDtseDP6mvZUtSFDvQz57Ci4EPcjVQmNijyJ8hCofKUyX
twCPRg7IZj6rAqIv7vHvWikI74G7nQKu3WDoAhkxPZDBVCd2nLLryETt5toYLgw16dF2dgCWuHB0
QiP4uC8vBYquPID/ITX1Z9IpdA1PnHtbAPc6qCZbpvGhn/uDD444C+Um08GsOvFqF6SAxBWn1SDn
JwSL2zRWl+Ncpe+S9+sSas5wvyKIXjbS8YMbpBtdPafOugBDLU+khKMZLaIXpm7x55Xr2nJM/SZo
UBzQIH7zxjG7ja4IYGsnbzQIO2eg4DZuHKqTR5tOTUvOT57VQSW7YlTzsfp/QotGCnxPb6rJ0E3b
fFKdRSR+lx8NN+mN5oIP616MRzsNULNJ1zcipZO/aoPdChFyuo9Q8mDvvt2boDDZNMWw/iG/nPXW
R3Z9vwaolsvrmye5sNRsH3Okv0pQrdg9Qt16v2J/J3UQr7VNuXiC7PixTMsxDC5LBMpbrIcs99HK
IMwNgKDEZN9QX3A9az4XMKZKDHvXixozX2gcrWTuqNiM7FXxTi/AIq/PE8gzpeSfKFV3QEksWxW+
Qq/TLsYrex+YlaWBLLe0uDuLEilgQz6FFRnx0M5QyWyXqT+7osIvRHFYJqoR3LzKvwJkKaMighjE
yhbgzC4QQ6h80SIQzsPjHIeZHxBMYwXyV5CQZAwGgGocGFMsVorQ6vVQBr+d/E3GYCtdTtyT1UJJ
XKyn/5E2z88VEQTq8iq3dfB6y4ebPiXlreIl1PVILM3sTPJFH6eRPeJT+CSL7OzUTMJfrN0DwhMT
uouQWMu6QH4xrVaXJRP1H93NWR094KAq81XR9KJPgkOCzmX9HqFMl/51OR6i3vhdKQmkfkBtmAZH
KIlDE4wIQvMHsQ3D3KPivijinwvmdFlsq+PHD1cnKbhAHhK3nled2pU5GpA1V87I1ezs7mG17IOi
qNlEy+QWr9M9e4QnFsVwUjMm9mq6OX9qg0wr1KH0V6TsFOecYu6Wd4wqg2DEbu2lFg8IpnEFeU3g
Oyb0CRHo2BMqCo1y1o3ufCGAm6l0cnJz3IlYtWon7bXVppqwAaxdm8xC/caCQdFN0NKevDN/X/Jm
qL2gTk5/sZjUVXM2vOv5UYDITM1WZt+8h+QtH3kQuYmTc8NBgSL10/rchL2saF8pfENNBfQ+Ye/g
SCTa0w1vZp7XftOxd/ZA1E7xyAb3JmZuUKDUYJpXMA+Y8JJaOJP/JxtHu2NSpxh2QbSYBAL+ZGhQ
Ar83mV+VEXOVu0aiLgQlvhc/kJUuAQp7/AN2B8bUJruVf9d+Fz+CgGWW9TyIIITw+au5T0QT3A6J
AvZ9z3Dlt56LkcDsF8vRfj6SNrcXY7nK5117mbi1EYtA44WNYk8Mbjy79Si0Pw1echjIyU1Bc07R
rHSkWZi193O/MD6H3/ERA5+QJ9pzYOo5j/xBYVzLb2xmNuQ0ADrQWPbSTQtJXpduW+nqWo0p608t
5ts/U5zX4PIeFJn/tVR+fGKRQj8NFKUFekwIEO2KeY3KSOD2dF3Zexa9IKSAxazmf9MhmVb8rccC
ajgwb6CEsgyY92dSYpb+6fvK5DQG+3que+nZi8971ZY88+gpAur5l1B18GF6cdx7JQKUahJwab62
YGGrY3X7UPgWhPsVUb1WcptH7yH4EWRdVixllNvQ4PnmNlLmzEfTOkKCFKbuKlS/QT3F83523Bkm
i4pM+L+kT7j/OgnebtWtQlWRQ7BvvvpupYTsWNObx1L4oYqjkRSgMEdxa8Eo193yvTVWXFjzywzW
9uldFB2+KmhFZhL7RL/LJitxN/4aRTekg2yxLsIrMntjp+iQDIyn4HYY/45Izy3sEswB2LfB/CbZ
LlfxyyNe2o/OUBkSHO8wuZ0JeWEGIIU2BCwiLHzxKOm5tuFI/epL2S05rvhpHDLOajFI/hksCgA5
gCeM5r2fUhO/xoy6fwjahuIZy4db72ENuRIeBE7pmpawapInt+1iXKvuJ6DMeSF4zOO8q1b0jCrH
Zyk7rqhqvR71eWs3XF72gckDBL4AP/OY+Yf77njKXy3itnAmt3EN8kJPNy8Xqo3cTE4XTMxst5jJ
V/AZRgeyxOmOteRtZSwhEdajFf7MxgN7K7Mu2LNT1MmwwuAxaCC7FaoKvUFf8x9Mq8saLIZnKJea
NOw09bXyrbcUpJUMkEGDcPvFT6tNiEHeVmA7ryc9gSociankwjhxFW17ZJqcUCunjBs5BgyjZk2u
iLtUIbicqiJDaIR7FZyvHGsosvIBXD7YB1KduZwmYPTeNzkKBUnRTLA46A/f5J4GOkhLDqbgm8a+
UWv57eqe8yL5c2ASfx0bm8s31YwCwH9D/+c4sAwF276khuJ+P1jWLGSSojqGkkYedoPrBb83R3N9
5Mj537XYYBFExWZARGSzCveRLsHJO9xQtiT2/hQlhv4je1Td4Z2OO8RoLb7EFBKKRXMxBTnsUHtV
jQ2svwRcPoKRjrTex60Dz51o4F1KmXn2NGIL94Cv8oDUMM7ty6B2/7TjTuTthv5czoGbvh2AE037
n/2PSJxwCEFnEtXuhqVGDHB6Yufo8FGR6WG+RX1hy8gDEAdXPNLzDuNDv+rI3tJwd2F00m3tkqy6
8aQkAHAaR2KIB/77n50Y2Xona1+hmA9br4Bp2f2Vqyz1XCVx8XNEGdR3iQ59SpZhjK9vSHvSL4uL
s+7dSaUwKWRIWr2fbxQ0Q9qD6dgv9f4QvOAWkEF+0Sqmunfcr5m6x7vG+8Dl7BrH1zQriv97+YFf
vLJHHw/JxHOF+SuNT8enbX1gPSRJY6yTVSvWJtQcLwcFIDkadCuHoYVmufygcaYgWsVjJ+Ig82S4
Lq8CxuoIe6mTv9HkV7PcEtD8ml9dSmOELmCEJKmIkIDZZOXA5OPbfj+CCrrx7enoAUvdyNUwmVYw
mILSBtlYNhlTmEUuEou3nHwdABz1IxQbNSao0TO8MSUmFxn1U18jCKYL3jnYD0wxKZSI1OgGR04s
sgJBhr48rwQcmnBocU7d/LnWyyozsge4tZOqNlg3ph/2QqPQ38KojtjdjH9rlua6b4LHg3//ofpH
Weqf01lFNZYlqLi8NI2uS/mrQgMwlDHMC1+6Dk86et9ObJPdszDmaVYjiqpBnMKS+QT5oxPUMLC/
J7s8f6TISr3N3zjnsto2u2IOB5nkI2/4cLIbV4d/gWQyr+aCAbPC6fLTlnBnerru9imCTij4J8Ef
8A2kzzPEPfAhd/V1fUUiJJKszqvcIedhv3V3q5u5+K3UgaArm8La9sjnubhvOWRE
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
